//
//  main.m
//  t0626_git_test_tt
//
//  Created by tt on 13-6-26.
//  Copyright (c) 2013年 tt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
