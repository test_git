//
//  AppDelegate.h
//  t0626_git_test_tt
//
//  Created by tt on 13-6-26.
//  Copyright (c) 2013年 tt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
